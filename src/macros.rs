macro_rules! printlog_info {
    ($($arg:tt)+) => (
        log::info!($($arg)+);
        println!($($arg)+);
    )
}

macro_rules! printlog_error {
    ($($arg:tt)+) => (
        log::error!($($arg)+);
        println!($($arg)+);
    )
}