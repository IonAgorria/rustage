use crate::utils::APP_CONFIG_MOUNT;
use crate::utils;

pub struct ConfigManager {
    mounted: bool
}

impl ConfigManager {
    pub(crate) fn new() -> Self {
        ConfigManager {
            mounted: false
        }
    }

    fn unmount_internal(&mut self) -> bool {
        if let Some(err) = utils::umount(APP_CONFIG_MOUNT, true).err() {
            self.mounted = false;
            println!("Error unmounting {}: {:?}", APP_CONFIG_MOUNT, err);
            utils::sleep(utils::APP_SLEEP_MEDITATION);
            return false;
        }
        self.mounted = false;
        return true;
    }

    fn mount_device(&mut self, path: &str) -> bool {
        if !path.is_empty() {
            if utils::wait_file(path, Some(0), false, true) {
                let (_, status) = utils::fork(true, || {
                    if let Some(e) = utils::run(&*format!("/bin/busybox mount {} {}", path, APP_CONFIG_MOUNT)).err() {
                        printlog_error!("Error mounting: {} {}", path, e);
                        utils::sleep(utils::APP_SLEEP_MEDITATION);
                    }
                    return 0;
                });
                if utils::success_fork_status(&*format!("mounting config device {}", path), status) {
                    self.mounted = true;
                    let mut app_path = std::path::PathBuf::from(APP_CONFIG_MOUNT);
                    app_path.push(utils::get_config_path());
                    if app_path.is_dir() {
                        printlog_info!("Success mounting config device: {}", path);
                        return true;
                    } else {
                        printlog_info!("Mounted config device but no app config dir exists: {} {:?}", path, app_path);
                        self.unmount_internal();
                    }
                }
            } else {
                printlog_error!("Config device not found: {}", path);
            }
        }
        return false;
    }

    pub fn is_mounted(&self) -> bool {
        self.mounted
    }

    pub fn unmount(&mut self) -> bool {
        if self.mounted {
            return self.unmount_internal();
        }
        return true;
    }

    pub fn mount(&mut self) -> bool {
        if !self.unmount() {
            return false;
        }
        let mut timeout = 5;
        while !self.mounted && 0 <= timeout {
            timeout -= 1;
            utils::mdev();
            for dev_path in utils::get_app_config_devices() {
                self.mount_device(dev_path);
                if self.mounted {
                    break;
                }
            }
        }
        if !self.mounted {
            printlog_info!("No config device configured");
        }
        return self.mounted;
    }
}