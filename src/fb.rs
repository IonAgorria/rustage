use crate::utils;
use crate::native;

pub fn open_fb() -> bool {
    println!("Opening framebuffer");
    if !utils::wait_file("/dev/fb0", Some(5), true, true) {
        return false;
    }

    //Create fork process for MSM FB refresh trick
    if unsafe { native::msm_fb_need() } == 1 {
        utils::fork(false, || {
            println!("Launching msm_fb_refresh");
            let ret = unsafe { native::msm_fb_refresh(1) };
            println!("Done msm_fb_refresh with: {}", ret);
            return 0;
        });
    }

    return true;
}

pub fn wrapper_fb() {
    utils::exec("fbpad", "/bin/sh").unwrap();
}
