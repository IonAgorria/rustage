#[macro_use]
pub mod macros;

pub mod native;
pub mod fb;
pub mod menu;
pub mod utils;
pub mod loader;
pub mod config;

use crate::loader::{Entry, EntryResult};
use std::io::BufRead;

fn main() {
    // Initialize the cursive logger.
    cursive::logger::init();

    let mut config_manager = config::ConfigManager::new();

    let pid1 = utils::is_pid1();
    if pid1 {
        //Mount and setup basic stuff
        println!("Mounting /sys");
        utils::mount("sysfs", "/sys", "sysfs")
        .expect("Error mounting /sys");

        println!("Mounting /proc");
        utils::mount("proc", "/proc", "proc")
        .expect("Error mounting /proc");

        println!("Setup /dev");
        utils::mdev();

        //Wait for console before taking control
        if !utils::wait_file(utils::get_console_path(), Some(10), true, true) {
            println!("Timeout waiting to console");
        }

        println!("Wiping FB");
        let (_, _status) = utils::fork(true, || {
            if let Some(e) = utils::run("cat /dev/zero > /dev/graphics/fb0").err() {
                printlog_error!("Error wiping fb: {}", e);
            }
            return 0;
        });

        println!("Launching take_console");
        utils::take_console();
        println!("Done take_console");

        printlog_info!("Attempting to mount config device");
        config_manager.mount();

        printlog_info!("Running init.sh");
        const APP_COPY_THIRDPARTY: Option<&'static str> = option_env!("APP_COPY_THIRDPARTY");
        let (_, status) = utils::fork(true, || {
            if let Some(e) = utils::run(&*format!("/etc/init.sh {} {}",
                utils::get_config_path(),
                APP_COPY_THIRDPARTY.unwrap_or(""))).err() {
                printlog_error!("Error launching init.sh: {}", e);
                utils::sleep(utils::APP_SLEEP_MEDITATION);
            }
            return 0;
        });
        if !utils::success_fork_status(&*format!("running init.sh"), status) {
            utils::sleep(utils::APP_SLEEP_MEDITATION);
        }

        if utils::get_app_adb() {
            printlog_info!("Launching ADB daemon");

            if let Some(e) = nix::unistd::mkdir("/dev/pts", nix::sys::stat::Mode::S_IRWXU).err() {
                printlog_error!("Error mkdir /dev/pts {}", e);
            }
            if let Some(e) = utils::mount("devpts", "/dev/pts", "devpts").err() {
                printlog_error!("Error mounting /dev/pts {}", e);
            }

            if utils::wait_file("/dev/android_adb", Some(0), true, true) {
                utils::fork(false, || {
                    if let Some(e) = utils::run(&*format!("/etc/adbd.sh")).err() {
                        printlog_error!("Error launching adbd: {}", e);
                        utils::sleep(utils::APP_SLEEP_MEDITATION);
                    }
                    return 0;
                });
            }
        }

        printlog_info!("Setup done");
    } else {
        printlog_info!("Not PID 1, skipping setup");
    }

    //Load args
    let nofb = utils::get_app_no_fb();
    let noterm = utils::get_app_no_term();
    let shell = std::env::args_os().any(|a| a == "-shell");

    //Load options
    let mut entry_choice: Entry = if pid1 || shell {
        Entry::Shell
    } else {
        Entry::Exit
    };
    let mut auto_boot = config_manager.is_mounted() || true;

    //Attempt to open FB and create menu in FB or CLI
    if !nofb {
        if fb::open_fb() {
            fb::wrapper_fb();
            entry_choice = Entry::Shell;
        }
    }

    //Main loop
    let mut entries;
    let mut entry_result;
    loop {
        //Load them, we still load after executing first time in case user changed configs
        entries = loader::load_entries();
        if auto_boot && !entries.is_empty() {
            entry_choice = entries.get(0).unwrap().clone();

            //Check if cmdline has entry selection, useful for chained rustage
            let data = match std::fs::File::open("/proc/cmdline") {
                Err(err) => {
                    printlog_error!("Error reading cmdline: {:?}", err);
                    utils::sleep(utils::APP_SLEEP_MEDITATION);
                    None
                }
                Ok(file) => {
                    std::io::BufReader::new(file).lines()
                    .map(|l| l.unwrap_or_else(|err| {
                        printlog_error!("Error reading cmdline lines: {:?}", err);
                        String::new()
                    }))
                    .next()
                }
            };

            if let Some(data) = data {
                let mut entry_title = "";

                //Cut cmdline part and then cut the last part separated by " "
                let pieces: Vec<&str> = data.split("rustage.entry=").collect();
                if pieces.len() == 2 {
                    let pieces_tail: Vec<&str> = pieces[1].split(" ").collect();
                    entry_title = pieces_tail[0];
                }

                //Match first entry with same title
                if let Some(entry_cmdline) = entries.iter().find(|e| {
                    match e {
                        Entry::Kernel(title, _, _, _) | Entry::Command(title, _) => {
                            title == entry_title
                        }
                        _ => false
                    }
                }) {
                    entry_choice = entry_cmdline.to_owned();
                }
            }
        }

        //Launch menu?
        if !noterm {
            if let Some(menu_choice) = menu::menu_term(&entries, &entry_choice, auto_boot) {
                entry_choice = menu_choice;
            } else {
                //No choice, reload config and menu
                config_manager.mount();
                continue;
            }
            auto_boot = false;
        }

        //Handle choice
        entry_result = loader::run_entry(&entry_choice);
        if entry_result != EntryResult::Loop {
            break;
        }
    }

    //Finale
    println!("Exiting, sayonara!");
    //We want to unmount stuff before doing anything
    config_manager.unmount();
    //Choose result action
    match entry_result {
        EntryResult::Kexec => {
            //Launch Kexec
            if let Some(e) = utils::run(utils::get_kexec_exec_command()).err() {
                println!("Error kexec {}", e);
            }

            //This should not be executed if kexec worked...
            println!("Something went wrong during kexec!");
            utils::sleep(utils::APP_SLEEP_MEDITATION);
            std::process::exit(1);
        },
        EntryResult::Reboot => {
            if let Some(e) = nix::sys::reboot::reboot(nix::sys::reboot::RebootMode::RB_AUTOBOOT).err() {
                printlog_error!("Error during reboot() syscall: {}", e);
            }
        },
        EntryResult::Poweroff => {
            if let Some(e) = nix::sys::reboot::reboot(nix::sys::reboot::RebootMode::RB_POWER_OFF).err() {
                printlog_error!("Error during reboot() syscall: {}", e);
            }
        },
        EntryResult::Exit => {
            //Normal exit
            std::process::exit(0);
        }
        EntryResult::Loop => {
            unreachable!();
        },
    }
}

