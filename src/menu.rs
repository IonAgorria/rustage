use crate::utils;
use crate::loader::Entry;
use log;

use cursive::event::{EventResult, Event, EventTrigger, Key};
use cursive::traits::*;
use cursive::views::{
    Dialog,
    OnEventView,
    SelectView
};
use cursive::Cursive;
use std::sync::mpsc::Sender;

pub fn menu_term(entries: &Vec<Entry>, prev_entry: &Entry, first_menu: bool) -> Option<Entry> {
    log::info!("Starting menu");

    //Create selector view
    let mut select: SelectView<Entry> = SelectView::new()
        .align(cursive::align::Align::center_left())// Center
        .autojump() // Use keyboard to jump to the pressed letters
    ;

    // Add selections
    if !entries.is_empty() {
        //Add entries
        for entry in entries {
            select.add_item(entry.label(), entry.clone());
        }
        //Preselect prev entry if available
        if let Some(i) = entries.iter().position(|e| e == prev_entry) {
            select.set_selection(i);
        } else {
            select.set_selection(0);
        }
    }

    // Sets the callback for when "Enter" is pressed.
    let (tx, rx) = std::sync::mpsc::channel();
    let tx_submit = tx.clone();
    select.set_on_submit(move |siv: &mut Cursive, entry: &Entry| {
        submit_entry(siv, &tx_submit, Some(entry.clone()));
    });

    // Sets the callback for selection change
    select.set_on_select(move |siv: &mut Cursive, _entry: &Entry| {
        //This disables timeout
        siv.set_fps(0);
    });

    //Wrap the select view with event view so we can add listeners
    let mut select = OnEventView::new(select);

    // Let's add some keys that change menu seleccion
    let trigger_up = EventTrigger::from_fn_and_tag(
        |e| match e {
            Event::Char('w') | Event::Key(Key::Up) => true,
            _ => false,
        },
        "trigger_up",
    );
    select = select.on_pre_event_inner(trigger_up, |s, _| {
        if let Some(index ) = s.selected_id() {
            let cb = if index == 0 {
                s.set_selection(s.len() - 1)
            } else {
                s.select_up(1)
            };
            return Some(EventResult::Consumed(Some(cb)));
        }
        None
    });
    let trigger_down = EventTrigger::from_fn_and_tag(
        |e| match e {
            Event::Char('s') | Event::Key(Key::Down) => true,
            _ => false,
        },
        "trigger_down",
    );
    select = select.on_pre_event_inner(trigger_down, |s, _| {
        if let Some(index ) = s.selected_id() {
            let cb = if index < s.len() - 1 {
                s.select_down(1)
            } else {
                s.set_selection(0)
            };
            return Some(EventResult::Consumed(Some(cb)));
        }
        None
    });

    // Create isntance
    let mut siv = cursive::default();
    if let Some(err) = siv.load_theme_file("./etc/style.toml").err() {
        log::error!("Error loading theme {:?}", err);
    }

    // Remove exit callbacks that are added by default
    siv.clear_global_callbacks(Event::CtrlChar('c'));
    //siv.clear_global_callbacks(Event::Exit);

    //Add callback for poweroff
    let tx_poweroff = tx.clone();
    siv.set_global_callback(Event::CtrlChar('q'), move |siv: &mut Cursive| {
        siv.set_fps(0);
        submit_entry(siv, &tx_poweroff, Some(Entry::Poweroff));
    });

    //Add callback for restart
    let tx_restart = tx.clone();
    siv.set_global_callback(Event::CtrlChar('a'), move |siv: &mut Cursive| {
        siv.set_fps(0);
        submit_entry(siv, &tx_restart, Some(Entry::Reboot));
    });

    //Add callback for debug console
    siv.set_global_callback(Event::CtrlChar('w'), move |siv: &mut Cursive| {
        siv.set_fps(0);
        siv.toggle_debug_console();
    });

    //Add callback for reloading menu
    let tx_reload = tx.clone();
    siv.set_global_callback(Event::CtrlChar('r'), move |siv: &mut Cursive| {
        siv.set_fps(0);
        submit_entry(siv, &tx_reload, None);
    });

    //Setup first menu stuff
    if first_menu && !entries.is_empty() {
        //Add callback per refresh
        let mut timeout = utils::get_auto_boot_secs();
        let default_entry = prev_entry.clone();
        siv.add_global_callback(Event::Refresh, move |siv: &mut Cursive| {
            if 0 < timeout {
                timeout -= 1;
            } else {
                submit_entry(siv, &tx, Some(default_entry.clone()));
            }
        });

        //Start refreshing once per second
        siv.set_fps(1);
    } else {
        siv.set_fps(0);
    }

    // Create dialog to wrap our menu
    siv.add_layer(
        Dialog::around(
            select.scrollable()
            .min_size((utils::APP_MENU_WIDTH, 2))
        )
        .title(utils::get_app_title())
    );

    siv.run();

    match rx.recv() {
        Ok(entry) => {
            entry
        },
        Err(e) => {
            printlog_error!("Error receiving entry: {}", e);
            Some(prev_entry.clone())
        }
    }
}

fn submit_entry(siv: &mut Cursive, tx: &Sender<Option<Entry>>, entry: Option<Entry>) {
    if let Some(err) = tx.send(entry).err() {
        printlog_error!("Error submitting to channel {:?}", err);
    } else {
        siv.quit();
    }
}

