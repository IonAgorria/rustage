use nix::unistd::{close, dup2, ForkResult, Pid};
use crate::native;
use std::ffi::CString;
use std::fs::OpenOptions;
use core::convert::Infallible;
use std::io::ErrorKind;
use nix::sys::wait::WaitStatus;

//Config device to try mounting
pub const APP_CONFIG_DEVS: Option<&'static str> = option_env!("APP_CONFIG_DEVS");
//Path for mounting config dev
pub const APP_CONFIG_MOUNT: &'static str = "/data";
//Min width of menu
pub const APP_MENU_WIDTH: usize = 20;
//Amount of time to wait after an error so the user can read
pub const APP_SLEEP_MEDITATION: u64 = 5000;

///Command to run when doing kexec load
pub fn get_kexec_load_command() -> &'static str {
    const APP_KEXEC_LOAD_COMMAND: Option<&'static str> = option_env!("APP_KEXEC_LOAD_COMMAND");
    APP_KEXEC_LOAD_COMMAND.unwrap_or("kexec -l")
}

///Command to run when doing kexec exec
pub fn get_kexec_exec_command() -> &'static str {
    const APP_KEXEC_EXEC_COMMAND: Option<&'static str> = option_env!("APP_KEXEC_EXEC_COMMAND");
    APP_KEXEC_EXEC_COMMAND.unwrap_or("kexec -e")
}

///Path for console device
pub fn get_console_path() -> &'static str {
    const APP_CONSOLE_PATH: Option<&'static str> = option_env!("APP_CONSOLE_PATH");
    APP_CONSOLE_PATH.unwrap_or("/dev/console")
}

///Get title of app to show
pub fn get_app_title() -> String {
    const PKG_VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
    "Rustage ".to_owned() + PKG_VERSION.unwrap_or("")
}

///Path to load config inside the mounted config dev
pub fn get_config_path() -> &'static str {
    const APP_CONFIG_PATH: Option<&'static str> = option_env!("APP_CONFIG_PATH");
    APP_CONFIG_PATH.unwrap_or("rustage/")
}

///Get amount of secs to wait before auto booting
pub fn get_auto_boot_secs() -> i32 {
    const APP_AUTO_BOOT_SECS: Option<&'static str> = option_env!("APP_AUTO_BOOT_SECS");
    APP_AUTO_BOOT_SECS.unwrap_or("3").parse::<i32>().unwrap_or(3)
}

///Get NO FB state
pub fn get_app_no_fb() -> bool {
    const APP_FB: Option<&'static str> = option_env!("APP_FB");
    APP_FB.is_none() || std::env::args_os().any(|a| a == "-nofb")
}

///Get NO Term state
pub fn get_app_no_term() -> bool {
    const APP_NO_TERM: Option<&'static str> = option_env!("APP_NO_TERM");
    APP_NO_TERM.is_some() || std::env::args_os().any(|a| a == "-noterm")
}

///Get ADB state
pub fn get_app_adb() -> bool {
    const APP_ADB: Option<&'static str> = option_env!("APP_ADB");
    APP_ADB.is_some() || !std::env::args_os().any(|a| a == "-noadb")
}

///Sleeps millis
pub fn sleep(millis: u64) {
    let delay = std::time::Duration::from_millis(millis);
    std::thread::sleep(delay)
}

///Forks current process to run f() code, waits optionally and returns child
pub fn fork<F>(wait: bool, f: F) -> (Pid, Option<WaitStatus>)
    where
        F: FnOnce() -> i32,
{
    let fork_result;
    unsafe {
        fork_result = nix::unistd::fork();
    };
    match fork_result {
        Ok(ForkResult::Parent { child, .. }) => {
            let mut result = None;
            if wait {
                result = Some(nix::sys::wait::waitpid(child, None).expect("waitpid failed"));
            }
            return (child, result);
        }
        Ok(ForkResult::Child) => {
            let code = f();
            std::process::exit(code);
        },
        Err(_) => panic!("Fork failed"),
    }
}

/// Handles fork return status and returns true if was successful termination
pub fn success_fork_status(title: &str, status: Option<WaitStatus>) -> bool {
    if let Some(WaitStatus::Exited(_, code)) = status {
        if code == 0 {
            return true;
        } else {
            printlog_error!("Exit code at {} {}", title, code);
        }
    } else {
        printlog_error!("Error launching {}", title);
    }
    return false;
}

/// Mounts device
pub fn mount(source: &str, target: &str, fstype: &str) -> nix::Result<()> {
    nix::mount::mount(
        Some(source),
        target,
        Some(fstype),
        nix::mount::MsFlags::empty(),
        None::<&str>
    )
}

/// Unmounts device
pub fn umount(path: &str, force: bool) -> nix::Result<()> {
    nix::mount::umount2(
        path,
        if force {
            nix::mount::MntFlags::MNT_FORCE
        } else {
            nix::mount::MntFlags::empty()
        }
    )
}

/// This causes a fork which calls busybox's mdev
pub fn mdev() {
    fork(true, || {
        nix::unistd::execv(CString::new("/bin/busybox").unwrap().as_c_str(),
           &[
               CString::new("/bin/mdev").unwrap().as_c_str(),
               CString::new("-s").unwrap().as_c_str()
           ]
        )
        .expect("Error launching mdev");
        println!("Executed mdev");
        return 0;
    });
    sleep(1000);
}

/// Replaces current process with busybox shell to launch the provided script/command
pub fn run(script: &str) -> nix::Result<Infallible> {
    nix::unistd::execv(CString::new("/bin/busybox").unwrap().as_c_str(),
       &[
           CString::new("/bin/sh").unwrap().as_c_str(),
           CString::new("-c").unwrap().as_c_str(),
           CString::new(script).unwrap().as_c_str()
       ]
    )
}

/// Replaces current process with provided binary
pub fn exec(binary: &str, args: &str) -> nix::Result<Infallible> {
    nix::unistd::execv(CString::new(binary).unwrap().as_c_str(),
       &[
           CString::new(args).unwrap().as_c_str()
       ]
    )
}


/// Launches shell
pub fn shell() -> nix::Result<Infallible> {
    exec("/bin/busybox", "/bin/sh")
}

/// Waits until specified file is available or time outs
pub fn wait_file(path: &str, mut timeout_secs: Option<i32>, call_mdev: bool, print_error: bool) -> bool {
    loop {
        //Timeout if reached under 0
        if let Some(timeout) = timeout_secs {
            if timeout < 0 {
                break;
            }
        }

        //Attempt to open it
        let file = OpenOptions::new()
            .read(true)
            .write(false)
            .create(false)
            .open(path.clone());
        match file {
            Ok(_) => {
                return true;
            }
            Err(e) => {
                if print_error {
                    printlog_error!("Error opening {} {:?}", path, e);
                }
                //Some errors are stupid to wait for so just break
                match e.kind() {
                    ErrorKind::PermissionDenied => {
                        break;
                    }
                    _ => {
                        let mut should_sleep = true;
                        if call_mdev {
                            mdev();
                            should_sleep = false;
                        }

                        //Countdown if available
                        if let Some(timeout) = timeout_secs {
                            if should_sleep && 0 >= timeout {
                                should_sleep = false;
                            }
                            timeout_secs = Some(timeout - 1);
                        }

                        if should_sleep {
                            sleep(1000);
                        }
                    }
                }
            }
        };
    }
    return false;
}

/// Does console takeover
pub fn take_console() {
    //SRC: kernel_chooser from tux-mind https://github.com/tux-mind/tf201-kernel/
    use nix::fcntl::OFlag;
    use nix::sys::stat::Mode;

    for i in 0..3 {
        if let Some(e) = close(i).err() {
            printlog_error!("Error closing {} {}", i, e);
        }
    }

    if let Some(e) = nix::unistd::setsid().err() {
        printlog_error!("Error at setsid {}", e);
    }

    let console_fd = match nix::fcntl::open(
        get_console_path(),
        OFlag::O_RDWR | OFlag::O_NOCTTY,
        Mode::S_IRWXU
    ) {
        Ok(fd) => fd,
        Err(e) => {
            printlog_error!("Error opening console {}", e);
            return;
        }
    };

    unsafe {
        let ret = native::ioctl_tiocsctty(console_fd);
        if ret != 0 {
            printlog_error!("Error tiocsctty {}", ret);
        }
    }

    for i in 0..3 {
        if let Some(e) = dup2(console_fd, 2 - i).err() {
            printlog_error!("Error dup2 {} {}", i, e);
        }
    }
}

/// Return true if this process is PID 1 (init)
pub fn is_pid1() -> bool {
    nix::unistd::getpid() == Pid::from_raw(1)
}

pub fn get_app_config_devices() -> Vec<&'static str> {
    APP_CONFIG_DEVS.unwrap_or("").split(",").collect()
}