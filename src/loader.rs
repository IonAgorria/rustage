use crate::utils;
use std::fs;
use std::io::BufRead;
use std::path::{Path, PathBuf};

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum EntryResult {
    Exit,
    Kexec,
    Reboot,
    Poweroff,
    Loop
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Entry {
    Exit,
    Kernel(String, String, String, bool), //title, kernel, kexec args, wait
    Command(String, String), //title, cmdline
    Reboot,
    Poweroff,
    Shell,
}

impl Entry {
    pub fn label(&self) -> String {
        let mut label = match self {
            Entry::Kernel(title, ..) | Entry::Command(title, ..) => {
                format!("{}", title)
            }
            _ => {
                format!("{:?}", self)
            }
        };
        while label.len() < utils::APP_MENU_WIDTH {
            label.push(' ');
        }
        label
    }

    pub fn is_custom(&self) -> bool {
        match self {
            Entry::Kernel(..) | Entry::Command(..) => true,
            _ => false,
        }
    }
}

///Scans a dir for config path and addes entries
fn scan_config_dir(entries: &mut Vec<Entry>, dir: &str) {
    let mut path = PathBuf::from(dir);
    path.push(utils::get_config_path());
    match fs::read_dir(path.clone()) {
        Err(err) => {
            if err.kind() == std::io::ErrorKind::NotFound {
                printlog_info!("Not found at: {:?}", path);
            } else {
                printlog_error!("Error reading dir: {:?} {:?}", path, err);
            }
        }
        Ok(dir_entries) => {
            //Extract paths from dir entries
            let mut dir_paths: Vec<PathBuf> = vec![];
            for dir_entry_r in dir_entries {
                match dir_entry_r {
                    Err(err) => {
                        printlog_error!("Error reading entry: {:?} {:?}", path, err);
                    }
                    Ok(dir_entry) => {
                        dir_paths.push(dir_entry.path());
                    }
                }
            }

            //Sort paths and create entries
            dir_paths.sort();
            for dir_path in dir_paths {
                if let Some(entry) = parse_file_entry(dir_path.as_path()) {
                    entries.push(entry);
                }
            }
        }
    }
}

///Attempts to parse the path
fn parse_file_entry(path: &Path) -> Option<Entry> {
    //Check if is a file
    if !path.is_file() {
        return None;
    }
    //Check if has proper ext
    match path.to_str() {
        None => {
            return None;
        }
        Some(name) => {
            if !name.ends_with(".txt") && !name.ends_with(".conf") {
                return None;
            }
        }
    }

    let data = match std::fs::File::open(path) {
        Err(err) => {
            printlog_error!("Error reading entry: {:?} {:?}", path, err);
            return None;
        }
        Ok(file) => {
            std::io::BufReader::new(file)
        }
    };

    let mut lines: Vec<String> = vec![];
    for result_line in data.lines() {
        match result_line {
            Err(err) => {
                printlog_error!("Error reading line: {:?} {:?}", path, err);
                return None;
            }
            Ok(line) => {
                lines.push(line);
            }
        }
    }

    if lines.len() < 2 {
        printlog_error!("File does not contain title or kind lines: {:?}", path);
        return None;
    }

    let title = lines.remove(0).clone();
    let kind_line = lines.remove(0).to_lowercase();
    let kind_parts: Vec<&str> = kind_line.split_whitespace().collect();
    //Interpret kind type
    match kind_parts.first().unwrap().to_string().as_str() {
        "kernel" | "boot" | "kexec" => {
            //Take kernel path
            if lines.is_empty() {
                printlog_error!("File does not contain kernel path line: {:?}", path);
                return None;
            }
            let kernel_path = lines.remove(0).clone();
            if kernel_path.is_empty() {
                printlog_error!("File does not contain kernel path: {:?}", path);
                return None;
            }
            //Take the leftovers as kexec args
            let args = if lines.is_empty() {
                String::new()
            } else {
                lines.join(" ")
            };
            //Check if we got wait param
            let wait = kind_parts.iter().any(|p| *p == "wait");
            Some(Entry::Kernel(title, kernel_path, args, wait))
        }
        "command" | "cmd" | "script" | "sh" | "exec" => {
            if lines.is_empty() {
                printlog_error!("File does not contain command to execute: {:?}", path);
                return None;
            }
            let command = lines.join(" ");
            Some(Entry::Command(title, command))
        }
        "exit" | "quit" => {
            Some(Entry::Exit)
        }
        "reboot" | "restart" => {
            Some(Entry::Reboot)
        }
        "shutdown" | "poweroff" => {
            Some(Entry::Poweroff)
        }
        "shell" => {
            Some(Entry::Shell)
        }
        _ => {
            printlog_error!("File has unknown kind: {:?} {}", path, kind_line);
            None
        }
    }
}

fn add_extra_entry(entries: &mut Vec<Entry>, entry: Entry) {
    if !entries.iter().any(|e| e == &entry) {
        entries.push(entry)
    }
}

pub fn load_entries() -> Vec<Entry> {
    let mut entries = vec![];

    //Load files
    scan_config_dir(&mut entries, utils::APP_CONFIG_MOUNT);
    scan_config_dir(&mut entries, "etc");

    //Add default entries
    if entries.is_empty() {
        add_extra_entry(&mut entries, Entry::Shell);
    }
    add_extra_entry(&mut entries, Entry::Reboot);
    add_extra_entry(&mut entries, Entry::Poweroff);
    if !utils::is_pid1() {
        add_extra_entry(&mut entries, Entry::Exit);
    }

    entries
}

pub fn run_entry(entry: &Entry) -> EntryResult {
    match entry {
        Entry::Kernel(title, kernel, args, wait) => {
            let command = format!("{} \"{}\" {}", utils::get_kexec_load_command(), kernel, args);
            printlog_info!("Kernel entry: '{}' - {}", title, command);
            if *wait {
                //Poll FS until kernel file is available
                utils::wait_file(
                    kernel.as_str(),
                    Some(30),
                    false,
                    false
                );
            }
            let (_, status) = utils::fork(true, || {
                if let Some(err) = utils::run(command.as_str()).err() {
                    println!("Error on kexec {:?}: {:?}", title, err);
                    return 1;
                }
                return 0;
            });

            if utils::success_fork_status("loading kexec", status) {
                //Loading was success, so we only need to launch our kernel
                return EntryResult::Kexec;
            } else {
                utils::sleep(utils::APP_SLEEP_MEDITATION);
            }
        }
        Entry::Command(title, command) => {
            printlog_info!("Command entry: '{}' - {}", title, command);
            let (_, status) = utils::fork(true, || {
                if let Some(err) = utils::run(command.as_str()).err() {
                    println!("Error on command {:?}: {:?}", title, err);
                    return 1;
                }
                return 0;
            });
            if !utils::success_fork_status("running command", status) {
                utils::sleep(utils::APP_SLEEP_MEDITATION);
            }
        }
        Entry::Shell => {
            utils::fork(true, || {
                if let Some(err) = utils::shell().err() {
                    println!("Error on shell: {:?}", err);
                    return 1;
                }
                return 0;
            });
        }
        Entry::Reboot => {
            return EntryResult::Reboot;
        }
        Entry::Poweroff => {
            return EntryResult::Poweroff;
        }
        Entry::Exit => {
            return EntryResult::Exit;
        }
    }
    return EntryResult::Loop;
}
