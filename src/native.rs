
///msm_fb.c
extern {
    pub fn ioctl_tiocsctty(fd: i32) -> i32;
    pub fn msm_fb_need() -> i32;
    pub fn msm_fb_refresh(do_loop: i32) -> i32;
}