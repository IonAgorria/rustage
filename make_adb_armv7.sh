#!/bin/bash
set -e

export APP_DEVICE=adb
export APP_ARCH=armv7

source ./common.sh

common_build
adb push $APP_TARGET_BIN /sdcard/init_$APP_ARCH
adb shell ln -s /dev/graphics/fb0 /dev/fb0
adb shell /sdcard/init_$APP_ARCH