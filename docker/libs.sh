#!/usr/bin/env bash

set -x
set -euo pipefail

apt-get install --assume-yes --no-install-recommends \
   libz-dev zlib1g-dev liblzma-dev
