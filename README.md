# Rustage

A basic initramfs based boot manager for linux/android based devices to assist kexec booting

## What
The project aims to create a simple boot menu as long as these criteria are met in target device:
- Linux kernel that boots until /init is executed
- Capable of loading this generated initramfs/initrd
- Linux has VT/terminal support compiled in
- Has filesystem drivers compiled for the partition FS that contains the configs/files
- Has some sort of keys input (GPIO based key events or physical keyboard for example)
- Kexec support enabled, some devices might require "hardboot" kexec workaround. Furthermore some old kernels that use
  ATAGs might need certain patches to load and kexec kernels that require DTB.

## Requirements
For running these scripts the following must be setup already in your host:
- Cross https://github.com/rust-embedded/cross and its requirements met
  - Provides already configured Rust cross compilation under container environment
- Docker
  - These scripts create custom cross Docker images, this assumes that cross will use Docker
    
You will also need to provide your own device Android boot.img file as template at binaries/ 
in case of using mkbootimg function.

## Device script
Each device has its own script(s) for calling common.sh functions and other device specific setup as desired,
on top of device script there should be some ENV vars that will assist the functions in generating
the proper initrd.

Once the script is created is only matter of running the created script.

## Menu config
On target device the partition must be accessible and mountable by Linux.

This partition will be auto-mounted at /data and kept mounted if the required "rustage" dir exists at root, otherwise
is unmounted and next partition at APP_CONFIG_DEVS list is checked.

A secondary init.sh is run for custom shell commands during rustage init if exists in rustage dir.

Inside folder the .txt/.conf files are scanned alphabetically and entry is added in menu if correct. The file must
follow the following pattern:
```
Menu entry title
Type of entry
Any amount of line/s
that will be concat
into one line
```

Examples:
- kernel | boot | kexec - Executes kexec-tools userland program to load files into memory and setup kernel
"wait" can be appended to "type of entry" so the kexec is not launched until file becomes available in path.
The command to run the base arguments can be adjusted in APP_KEXEC_LOAD_COMMAND.
```
Example PMOS DTB kernel
Kernel
/data/kernel/pmos
--dtb=/data/dtb/pmos
--initrd=/data/initrd/pmos
--append="earlyprintk loglevel=4 console=tty0
the rest
of cmdline"
```

```
Example Android ATAGs kernel
Kernel
/data/kernel/android
--atags
--initrd=/data/initrd/android
--append="earlyprintk loglevel=4 console=tty0 ..."
```

```
RAM DTB which waits kernel file to exist
kexec wait
/kernel
--dtb=/dtb
--initrd=/initrd
--append="earlyprintk loglevel=4 console=tty0 ..."
```
  
- command | cmd | script | sh | exec - Executes a shell command after concatenating all lines into one
```
Example shell command
Command
echo "testing long msg"
&& sleep 1
&& echo "done"
```

- exit | quit - Stops program execution (added automatically if not PID1, can be reordered)
```
Quit program
Exit
```

- reboot | restart - Sends reboot order to kernel (added automatically, can be reordered)
```
Reboot device
reboot
```

- shutdown | poweroff - Sends poweroff order to kernel (added automatically, can be reordered)
```
Poweroff device
poweroff
```

- shell - Shows shell for commands (added automatically if no entries, can be reordered)
```
Shell
shell
```

## Local config.sh
Users can create a config.sh file locally that won't be uploaded to git, this might be useful to have own
configs/paths/tweaks that are not applicable outside such as:

```shell
export APP_AUTO_BOOT_SECS=2
export BLOBPACK_BIN=$THIS/../tf201/BlobTools/blobpack

if [[ $APP_DEVICE == "tf201" ]]; then
  export KERNEL_CUSTOM_IMAGE=$THIS/binaries/tf201_current_kernel
fi
```

## ENVs
These are some of envs available to tweak, some might be undocumented so searching it in source is advisable.

Required:
- APP_DEVICE - The target device key to use, this will be used for device specific files such as boot.img file.
- APP_ARCH - Architecture of target device, such as x86_64, armv7, aarch64 ... https://github.com/rust-embedded/cross#supported-targets

Tweaks:
- APP_AUTO_BOOT_SECS - How many seconds to wait until first option is chosen, code uses default value unless set 
- APP_CONFIG_DEVS - /dev paths and order to check separated by comma when attempting to mount the partition containing
  rustage configs. Such as /dev/mmcblk1p1,/dev/mmcblk0p1,/dev/sda1 (First sdcard, eMMC and USB).
  If partition doesn't contain rustage folder at root it will be skipped and next partition will be attempted.
- APP_ADB - Flag for compiling adbd binary and add support for launching during init
- APP_COPY_THIRDPARTY - Path inside device containing binaries (Example /data/rustage/bin) such as kexec, adbd
  by default thirdparty binaries are bundled unless this ENV is set.
  This can be used to reduce initrd size if doesn't fit by offloading certain binaries to external partition.
- APP_MSM_FB - Use Qualcomm/MSM framebuffer refresh workaround
- KERNEL_CMDLINE_PRE - cmdline to append before the original cmdline specified in boot.img (if any)
- KERNEL_CMDLINE_POST - cmdline to append after the original cmdline specified in boot.img (if any)
- KERNEL_INITRD_COMPRESSION - Custom compression command for initrd cpio, used to reduce initrd size. Kernel
  must have this compression format support compiled in. Gzip by default unless specified.
- APP_KEXEC_LOAD_COMMAND - Command to launch as base when running a "kexec" type of menu entry, one example is
  "kexec -a --mem-min=0x85000000 --load-hardboot" for tf201 device which uses kexec-hardboot workaround and images
  must be written after 0x85000000 address

Build time:
- APP_CLEAN - Do not cleanup build directories when set as 0, used for quicker compilation but might result in stale
  env configurations being used if binary is not recompiled.
- APP_DEBUG - Create debug Rust binary.

Expert:
- APP_KEXEC_EXEC_COMMAND - Command to launch when executing the currently loaded kernel in memory.
- KERNEL_CUSTOM_IMAGE - Custom kernel image to use when assembling boot.img instead of reusing the one contained in
  template boot.img
- APP_CONFIG_PATH - Path where rustage configs directory is checked and scanned, default is used unless set.
- APP_NO_TERM - Directly boots first entry without showing menu.
- APP_FB - WIP attempt to create terminal on framebuffer when device kernel VT support is not functional. Do not use.
