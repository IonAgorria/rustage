#!/bin/sh

echo "Busybox install"
busybox --install

echo "Disabling printk"
sysctl -w kernel.printk="1 4 1 4"

if [[ ! -z $1 ]]; then
  echo "Running secondary init.sh"
  if [[ -f "/data/$1/init.sh" ]]; then
    . /data/$1/init.sh
  fi
  shift
fi

if [[ ! -z $1 ]]; then
  echo "Copying thirdparty binaries"
  cp -v $1/* /usr/bin
  chmod a+rx /usr/bin/*
  shift
fi

exit 0
