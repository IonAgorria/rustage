#!/bin/sh

#Handles the execution of ADB daemon

echo 0 > /sys/class/android_usb/android0/enable
echo "adb" > /sys/class/android_usb/android0/functions
echo 1 > /sys/class/android_usb/android0/enable

adbd &
