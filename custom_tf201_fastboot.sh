#!/bin/bash
set -e

SCRIPT="$(readlink -f $0)"
THIS="$(dirname $SCRIPT)"
cd $THIS

#Copy args before calling common.sh
if [ -z $1 ]; then
	echo "Need boot.img path"
	exit 1
fi
BOOTIMG_FILE=$1
shift
if [ -z $1 ]; then
	echo "Need kernel path"
	exit 1
fi
KERNEL_FILE=$1
shift

source ./common.sh

export BOOTIMG_CUSTOM_IMAGE=$BOOTIMG_FILE
export KERNEL_CUSTOM_IMAGE=$KERNEL_FILE

common_cleanup
common_mkbootimg
common_fastboot_blobpack
