#!/bin/bash
set -e

export APP_DEVICE=qemu
export APP_ARCH=armv7

source ./common.sh

common_build
common_qemu_arm_virt
