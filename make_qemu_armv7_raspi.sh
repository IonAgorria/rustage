#!/bin/bash
set -e

export APP_DEVICE=qemu
export APP_ARCH=armv7
export APP_CONFIG_DEVS=/dev/mmcblk0p2,/dev/sda1

source ./common.sh

common_build
common_qemu_arm_raspi
