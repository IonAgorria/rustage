#!/bin/sh

export APP_DEVICE=p895
export APP_ARCH=armv7
export APP_CONFIG_DEVS=/dev/mmcblk0p3,/dev/mmcblk0p4,/dev/mmcblk0p8
export APP_KEXEC_LOAD_COMMAND="kexec -a --mem-min=0x85000000 --load-hardboot"
export KERNEL_CMDLINE_PRE="loglevel=3 tegra_wdt.heartbeat=30 mem=1022M@2048M tegra_fbmem=4098560@0xabe01000 gpt console=tty1"
export APP_ADB=1
export KERNEL_INITRD_COMPRESSION="lzma -f"
