#!/bin/bash
set -e

export APP_DEVICE=qemu
export APP_ARCH=x86_64
export QEMU_CMDLINE="rustage.entry=miaumiau miau"
#export APP_CONFIG_DEVS=/dev/sda1

source ./common.sh

common_build
common_qemu_x86_64
