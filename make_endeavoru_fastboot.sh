#!/bin/bash
set -e

source ./config_endeavoru.sh
source ./common.sh

common_build
common_boot_img
common_fastboot_flash
fastboot reboot
