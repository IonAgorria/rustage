#!/bin/bash
set -e

export APP_MSM_FB=yes
export APP_DEVICE=bacon
export APP_ARCH=armv7
export KERNEL_CMDLINE_PRE="console=tty1"
export APP_CONFIG_DEVS=/dev/mmcblk0p1,/dev/sda1

source ./common.sh

common_build
common_boot_img
common_fastboot_boot
