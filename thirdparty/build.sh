#!/bin/bash
set -e

SCRIPT="$(readlink -f $0)"
THIS="$(dirname $SCRIPT)"
cd $THIS

#Setup compiler info
APP_CC_LIBC=gnu
case "$APP_ARCH" in
    arm*)
        APP_CC_ARCH=arm
        APP_CC_LIBC=gnueabihf
        ;;
    i*86)
        APP_CC_ARCH=i386
        ;;
    x86_64 | aarch64)
        APP_CC_ARCH=$APP_ARCH
        ;;
    *)
        echo "ERROR: unknown arch $APP_ARCH"
        exit 1
        ;;
esac
APP_TARGET_THIRDPARTY="$APP_CC_ARCH-linux-$APP_CC_LIBC"

function build_adbd() {
  echo build_adbd
  if [[ -d $THIS/adbd ]]; then
    if [[ $APP_CLEAN ]]; then
      cd $THIS/adbd/libcutils
      make clean
      cd $THIS/adbd/adb
      make clean
    fi

    cd $THIS/adbd/libcutils
    CROSS_COMPILE="$APP_TARGET_THIRDPARTY-" make
    cd $THIS/adbd/adb
    CROSS_COMPILE="$APP_TARGET_THIRDPARTY-" make
  fi
}

function build_kexec() {
  echo build_kexec
  if [[ -z $APP_KEXEC_CUSTOM ]]; then
    cd $THIS/kexec-tools
    ./bootstrap
    # Cleanup any prev config (specially when changing arch) since running configure again can give error
    if [[ $APP_CLEAN ]]; then
      make clean || true
    fi
    ./configure -prefix=/usr \
      --host="$APP_TARGET_THIRDPARTY" \
      --build="$HOST" \
      --with-zlib --with-lzma \
      LDFLAGS="-static -fcommon"
    if [[ $APP_CLEAN ]]; then
      make clean
    fi
    make targets
  else
    cd $THIS/kexec_$APP_DEVICE
    if [[ $APP_CLEAN ]]; then
      make clean
    fi
    CROSS_COMPILE="$APP_TARGET_THIRDPARTY-" make
  fi
}

function build_fbpad() {
  echo build_fbpad
  if [[ -d $THIS/fbpad ]]; then
    cd $THIS/fbpad
    if [[ $APP_CLEAN ]]; then
      make clean
    fi
    CC="$APP_TARGET_THIRDPARTY-gcc" make
  fi
}

function build_all() {
  if [[ ! -z $APP_ADB ]]; then
    build_adbd
  fi
  if [[ ! -z $APP_FB ]]; then
    build_fbpad
  fi
  build_kexec
}

#Run a method if requested
if [[ -n "$1" ]]; then
  METHOD=$1
  shift
  eval "$METHOD $*"
  exit 0;
else
  build_all
fi