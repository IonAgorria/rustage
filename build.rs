
fn main() {
    let includes: Vec<&str> = vec![];

    //Load ENVs
    let has_msm_fb = !std::env::var("APP_MSM_FB").unwrap_or_default().is_empty();

    //Compile utils lib
    let mut build = cc::Build::new();
    build.static_flag(true)
        .includes(includes.clone())
        .file("native/utils.c");

    if has_msm_fb {
        build.define("APP_MSM_FB", "1");
    }

    build.compile("utils");
} 
