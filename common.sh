#!/bin/bash
set -e

SCRIPT="$(readlink -f $0)"
THIS="$(dirname $SCRIPT)"
cd $THIS

export BUILD_DIR=$THIS/build
export BINARIES_DIR=$THIS/binaries
export THIRDPARTY_DIR=$THIS/thirdparty
export INITRD_DIR=$THIS/initramfs

#Setup targets
if [[ -z "$APP_TARGET" ]]; then
  export APP_TARGET="$APP_ARCH-unknown-linux-musl"
  if [[ $APP_ARCH = arm* ]]; then
    export APP_TARGET="$APP_TARGET"eabihf
  fi
fi
if [[ -z "$APP_TARGET_THIRDPARTY" ]]; then
  export APP_TARGET_THIRDPARTY="$APP_ARCH-unknown-linux-gnu"
  if [[ $APP_ARCH = arm* ]]; then
    export APP_TARGET_THIRDPARTY="$APP_TARGET_THIRDPARTY"eabihf
  fi
fi

#Load custom config or use default
if [[ -f config.sh ]]; then
  source config.sh
fi
if [[ -z $MKBOOTIMG_DIR ]]; then
  export MKBOOTIMG_DIR=$THIS/../mkbootimg
fi

#Set debug if qemu or ADB
if [[ -z $APP_DEBUG ]]; then
  if [[ $APP_DEVICE == "qemu" || $APP_DEVICE == "adb" ]]; then
    export APP_DEBUG=1
  fi
fi
if [[ $APP_DEBUG == 1 ]]; then
  echo "Debug mode"
  if [[ $APP_DEBUG_BUILD == 1 ]]; then
    echo "Debug build"
  else
    unset APP_DEBUG_BUILD
  fi
else
  unset APP_DEBUG
  unset APP_DEBUG_BUILD
fi

#Force clean if not debug
if [[ -z $APP_CLEAN ]]; then
  if [[ -z $APP_DEBUG ]]; then
    export APP_CLEAN=1
  fi
fi
if [[ $APP_CLEAN == 1 ]]; then
  echo "Clean before build"
else
  unset APP_CLEAN
fi

#Unset APP_FB if APP_NO_TERM is set
if [[ -n $APP_NO_TERM ]]; then
  unset APP_FB
fi

#Functions
function common_setup_docker() {
  echo "common_setup_docker"

  #Handle per arch
  case "$APP_ARCH" in
      arm*)
          DOCKER_ARCH=arm
          ;;
      aarch64)
          DOCKER_ARCH=arm
          ;;
      i*86)
          DOCKER_ARCH=x86
          ;;
      x86_64)
          DOCKER_ARCH=x86
          ;;
      *)
          echo 'ERROR: unknown arch $APP_ARCH'
          exit 1
          ;;
  esac

  #Make docker for building binary
  CROSS_VER=$(cross -V | sed -nr 's/cross (.*)/\1/p')
  mkdir "$BUILD_DIR/docker"
  echo "FROM rustembedded/cross:$APP_TARGET-$CROSS_VER" > "$BUILD_DIR/docker/Dockerfile"
  docker build -t "custom_cross:$APP_TARGET" "$BUILD_DIR/docker"

  #Make docker for building thirdparty stuff
  docker build -t "custom_cross:$APP_TARGET_THIRDPARTY" "$THIS/docker" -f "$THIS/docker/Dockerfile.$DOCKER_ARCH"
}

function common_cleanup() {
  echo "common_cleanup"
  rm -rf "${BUILD_DIR:?}"/* || true
  mkdir -p "$BUILD_DIR/"
  touch "$BUILD_DIR/.noempty"
}

function common_download_deps() {
  echo "common_download_deps"
  cd $THIRDPARTY_DIR
  if [[ ! -d "$THIRDPARTY_DIR/kexec-tools" ]]; then
    git clone "git://git.kernel.org/pub/scm/utils/kernel/kexec/kexec-tools.git" -b "v2.0.20"
    cd kexec-tools
    #Apply patches
    git apply ../patches/kexec-tools-Add-kexec-hardboot-support.patch
    git apply ../patches/kexec-tools-Add-linux-musl-as-compatible-target.patch
  fi
  cd $THIS
}

function common_build_binary() {
  if [[ $APP_CLEAN ]]; then
    cross clean --target $APP_TARGET
  fi

  if [[ $APP_DEBUG_BUILD ]]; then
    echo "Calling cross debug"
    cross build --target $APP_TARGET
    export APP_TARGET_BIN=$THIS/target/$APP_TARGET/debug/rustage
  else
    echo "Calling cross release"
    cross build --release --target $APP_TARGET
    export APP_TARGET_BIN=$THIS/target/$APP_TARGET/release/rustage
  fi
}

# shellcheck disable=SC2120
function common_build_thirdparty() {
  echo "common_build_thirdparty $*"

  #Launch docker to build the thirdparty stuff using glibc (because kexec-tools doesnt work with musl)
  docker run \
  -v "$THIRDPARTY_DIR":/project \
  -w /project \
  -e APP_ARCH \
  -e APP_DEVICE \
  -e APP_TARGET \
  -e APP_TARGET_THIRDPARTY \
  -e APP_KEXEC_CUSTOM \
  -e APP_CLEAN \
  -e APP_ADB \
  -e APP_FB \
  -e APP_DEBUG \
  "custom_cross:$APP_TARGET_THIRDPARTY" \
  /project/build.sh "$@"
}

function common_assemble_initrd() {
  echo "common_assemble_initrd"

  #Copy the initramfs template
  cp -a "$INITRD_DIR" "$BUILD_DIR/initramfs"

  #Copy main app binary as init
  if [[ -z "$APP_TARGET_BIN" ]]; then
    echo "Binary path not set"
    exit 1;
  fi
  cp -a "$APP_TARGET_BIN" "$BUILD_DIR/initramfs/init"

  #Copy busybox, this is need for basic stuff
  cp -a "$THIRDPARTY_DIR/busybox/busybox-$APP_ARCH" "$BUILD_DIR/initramfs/bin/busybox"

  #Copy thirdparty binaries
  if [[ -z $APP_COPY_THIRDPARTY ]]; then
    APP_THIRDPARTY_DESTINATION="$BUILD_DIR/initramfs/bin"
  else
    APP_THIRDPARTY_DESTINATION="$BUILD_DIR/bin"
    mkdir "$APP_THIRDPARTY_DESTINATION"
  fi
  if [[ ! -z $APP_ADB ]]; then
    cp -a "$THIRDPARTY_DIR/adbd/adb/adbd" "$APP_THIRDPARTY_DESTINATION/adbd"
  fi
  if [[ ! -z $APP_FB ]]; then
    cp -a "$THIRDPARTY_DIR/fbpad/fbpad" "$APP_THIRDPARTY_DESTINATION/fbpad"
  fi
  if [[ -z "$APP_KEXEC_CUSTOM" ]]; then
    cp -a "$THIRDPARTY_DIR/kexec-tools/build/sbin/kexec" "$APP_THIRDPARTY_DESTINATION/kexec"
  else
    cp -a "$THIRDPARTY_DIR/$APP_DEVICE/kexec_$APP_DEVICE" "$APP_THIRDPARTY_DESTINATION/kexec"
  fi

  #Ensure they are executable
  chmod a+rx \
    $BUILD_DIR/initramfs/init \
    $BUILD_DIR/initramfs/etc/*.sh \
    $BUILD_DIR/initramfs/bin/* \
    $APP_THIRDPARTY_DESTINATION/*

  #Package the initramfs into CPIO
  cd "$BUILD_DIR/initramfs"
  find . | cpio --create --format='newc' > ../initrd_cpio

  #Setup default compression as GZIP
  if [[ -z "$KERNEL_INITRD_COMPRESSION" ]]; then
    export KERNEL_INITRD_COMPRESSION="gzip -f"
  fi

  cd "$BUILD_DIR"
  if [[ -n $KERNEL_INITRD_COMPRESSION && $KERNEL_INITRD_COMPRESSION == "" ]]; then
    #No compression wanted, just move the cpio as result
    mv initrd_cpio initrd.img
  else
    #Use the command and move the whatever extension file as result
    eval "$KERNEL_INITRD_COMPRESSION initrd_cpio"
    mv initrd_cpio.* initrd.img
  fi
  #Done
  cd $THIS
}

function common_mkbootimg() {
  echo "common_mkbootimg"
  mkdir -p "$BUILD_DIR/bootimg"
  
  if [[ $BOOTIMG_CUSTOM_IMAGE ]]; then
    echo "Using custom boot.img from $BOOTIMG_CUSTOM_IMAGE"
	cp "$BOOTIMG_CUSTOM_IMAGE" $BUILD_DIR/bootimg/boot.img
  else
	if [[ -z $APP_DEVICE ]]; then
		echo "Missing vars: APP_DEVICE"
		exit 1
	fi
	cp "$BINARIES_DIR/$APP_DEVICE.img" $BUILD_DIR/bootimg/boot.img
  fi
  cd "$BUILD_DIR/bootimg"

  #Unpack img
  $MKBOOTIMG_DIR/unpackbootimg -i "$BUILD_DIR/bootimg/boot.img" -o "$BUILD_DIR/bootimg"

  #Check if has some extra stuff
  MKBOOTIMG_EXTRAS=""

  #Handle Ramdisk
  if [[ -f "$BUILD_DIR/initrd.img" ]]; then
    MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --ramdisk $BUILD_DIR/initrd.img"
  else
    echo "Warning: no custom initrd.img available, using original ramdisk"
    if [[ -f "boot.img-ramdisk" ]]; then
      MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --ramdisk boot.img-ramdisk"
    else
      MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --ramdisk boot.img-ramdisk.*"
    fi
  fi

  #Handle DT
  if [[ -f "boot.img-dt" ]]; then
    MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --dt boot.img-dt"
  fi

  #Handle DTB
  if [[ $DTB_CUSTOM ]]; then
    echo "Using custom kernel from $DTB_CUSTOM"
    rm "boot.img-dtb"
    cp "$DTB_CUSTOM" "boot.img-dtb"
  fi
  if [[ -f "boot.img-dtb" ]]; then
    MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --dtb boot.img-dtb"
  fi
  if [[ -f "boot.img-dtb_offset" ]]; then
    MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --dtb_offset $(cat boot.img-dtb_offset)"
  fi

  #Inject custom kernel if available
  if [[ -n $KERNEL_EMPTY ]]; then
    echo "Using empty kernel"
    rm -fv "boot.img-zImage"
    touch "boot.img-kernel"
    MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --kernel boot.img-kernel"
  else
    if [[ $KERNEL_CUSTOM_IMAGE ]]; then
      echo "Using custom kernel from $KERNEL_CUSTOM_IMAGE"
      rm -fv "boot.img-zImage"
      MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --kernel $KERNEL_CUSTOM_IMAGE"
    elif [[ ! -f "boot.img-zImage" ]] && [[ -f "boot.img-kernel" ]]; then
      MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --kernel boot.img-kernel"
    else
      MKBOOTIMG_EXTRAS="$MKBOOTIMG_EXTRAS --kernel boot.img-zImage"
    fi
  fi

  KERNEL_CMDLINE="$(cat boot.img-cmdline)"
  if [[ -n "$KERNEL_CMDLINE_PRE" ]]; then
    KERNEL_CMDLINE="$KERNEL_CMDLINE_PRE $KERNEL_CMDLINE"
  fi
  if [[ -n "$KERNEL_CMDLINE_PRE" ]]; then
    KERNEL_CMDLINE="$KERNEL_CMDLINE $KERNEL_CMDLINE_POST"
  fi

  echo "cmdline: $KERNEL_CMDLINE"

  #Repack with our ramdisk
  $MKBOOTIMG_DIR/mkbootimg \
    --cmdline "$KERNEL_CMDLINE" \
    --board "$(cat boot.img-board)" \
    --base "$(cat boot.img-base)" \
    --pagesize "$(cat boot.img-pagesize)" \
    --kernel_offset "$(cat boot.img-kernel_offset)" \
    --ramdisk_offset "$(cat boot.img-ramdisk_offset)" \
    --second_offset "$(cat boot.img-second_offset)" \
    --tags_offset "$(cat boot.img-tags_offset)" \
    --hashtype "$(cat boot.img-hashtype)" \
    $MKBOOTIMG_EXTRAS \
    --output "$BUILD_DIR/boot.img"
  cd "$THIS"
}

function common_build() {
  echo "common_build"
  common_cleanup
  common_download_deps
  common_setup_docker
  common_build_binary
  common_build_thirdparty
}

function common_boot_img() {
  echo "common_boot_img"
  common_assemble_initrd
  common_mkbootimg
}

function common_adb() {
  common_assemble_initrd
  common_mkbootimg

  #Set default ADB serial if none
  if [[ -z $ANDROID_SERIAL ]]; then
    export ANDROID_SERIAL=0123456789ABCDEF
  fi

  echo "Waiting for ADB device $ANDROID_SERIAL"
  adb wait-for-device

  #Copy stuff
  adb push "$BUILD_DIR/bootimg/boot.img-kernel" /data/kernel/kernel
  adb push "$BUILD_DIR/initrd.img" /data/initrd/rustage
  if [[ ! -z $APP_COPY_THIRDPARTY ]]; then
    adb push "$BUILD_DIR/bin"/* $APP_COPY_THIRDPARTY
  fi

  #Create symlink so device can load if it's waiting
  adb shell ln -s /data/kernel/kernel /kernel
}

function common_fastboot_blobpack() {
	if [[ -z "$BLOBPACK_BIN" ]]; then export BLOBPACK_BIN=../BlobTools/blobpack; fi
	if [[ -n "$APP_RECOVERY" ]]; then
	  $BLOBPACK_BIN -s $BUILD_DIR/boot.blob SOS $BUILD_DIR/boot.img
	else
	  $BLOBPACK_BIN -s $BUILD_DIR/boot.blob LNX $BUILD_DIR/boot.img
	fi
	if [[ -n "$APP_RECOVERY" ]]; then
	  echo "!!! Flashing recovery !!!"
	  fastboot flash recovery $BUILD_DIR/boot.blob
	else
	  fastboot flash boot $BUILD_DIR/boot.blob
	fi
}

function common_fastboot_flash() {
	if [[ -n "$APP_RECOVERY" ]]; then
	  echo "!!! Flashing recovery !!!"
	  fastboot flash recovery $BUILD_DIR/boot.img
	else
	  fastboot flash boot $BUILD_DIR/boot.img
	fi
}

function common_fastboot_boot() {
	fastboot boot $BUILD_DIR/boot.img
}

function common_qemu_arm_virt() {
  common_assemble_initrd
  qemu-system-arm \
    -m 256 \
    -kernel $BINARIES_DIR/vmlinuz.arm \
    -initrd $BUILD_DIR/initrd.img \
    -append "$QEMU_CMDLINE" \
    -serial stdio \
    -sd testing.img \
    -machine virt
}

function common_qemu_arm_raspi() {
  common_assemble_initrd
  qemu-system-arm \
    -kernel $BINARIES_DIR/vmlinuz.arm_raspi \
    -initrd $BUILD_DIR/initrd.img \
    -append "console=ttyAMA0 console=tty1 $QEMU_CMDLINE" \
    -no-reboot \
    -serial stdio \
    -drive file=testing.img,media=disk,if=sd,format=raw \
    -cpu arm1176 -m 1024 \
    -machine raspi2 -dtb binaries/bcm2709-rpi-2-b.dtb \
    -usb -device usb-kbd \
    #-net nic -net user
}

function common_qemu_x86_64() {
  common_assemble_initrd
  qemu-system-x86_64 \
    -kernel $BINARIES_DIR/vmlinuz.amd64 \
    -initrd $BUILD_DIR/initrd.img \
    -serial stdio \
    -append "console=ttyS0 console=tty1 vga=795 $QEMU_CMDLINE" \
    -machine q35,accel=kvm \
    -smp sockets=1,cpus=1,cores=2,maxcpus=2 -cpu host \
    -m 512 \
    -device virtio-blk-pci,drive=image \
    -drive if=none,id=image,file=testing.img,format=raw
}

#Run a method if requested
if [[ -n "$1" ]]; then
  METHOD=$1
  shift
  eval "$METHOD $*"
  exit 0;
fi
