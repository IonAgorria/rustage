#!/bin/bash
set -e

source ./config_tf201.sh
source ./common.sh

common_build
common_boot_img
common_fastboot_blobpack
fastboot reboot
