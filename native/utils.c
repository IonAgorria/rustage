
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <linux/fb.h>
#include <assert.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

int ioctl_tiocsctty(int fd) {
    return ioctl(fd, TIOCSCTTY, 1);
}

int msm_fb_need()
{
#if APP_MSM_FB
    return 1;
#endif
    return 0;
}

//Taken from msm-fb-refresher SRC: https://github.com/AsteroidOS/msm-fb-refresher/
int msm_fb_refresh(int loop)
{
    int ret = 0;

    int fd = open("/dev/fb0", O_RDWR);
    if (fd < 0) {
        return 1;
    }

    setpriority(PRIO_PROCESS, 0, -20);

    struct fb_var_screeninfo var;
    if (ioctl(fd, FBIOGET_VSCREENINFO, &var) < 0) {
        return 2;
    }

    if (loop) {
        while (1) {
            ioctl(fd, FBIOPAN_DISPLAY, &var);
            usleep(16666);
        }
    } else {
        ret = ioctl(fd, FBIOPAN_DISPLAY, &var);
        if (ret < 0) {
            perror("Failed FBIOPAN_DISPLAY");
            ret = 3;
        }
    }

    close(fd);

    return ret;
}
