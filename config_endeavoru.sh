#!/bin/sh

export APP_DEVICE=endeavoru
export APP_ARCH=armv7
export APP_CONFIG_DEVS=/dev/mmcblk0p13,/dev/sda1
export APP_KEXEC_LOAD_COMMAND="kexec -a --mem-min=0x85000000 --load-hardboot"
#export KERNEL_CMDLINE_PRE="loglevel=6 mem=1016M@2048M tegra_fbmem=3689280@0xbc0ab000 gpt"
#export KERNEL_CMDLINE_PRE="$KERNEL_CMDLINE_PRE console=tty1"
#export KERNEL_CMDLINE_PRE="$KERNEL_CMDLINE_PRE earlyprintk"
#export KERNEL_CMDLINE_PRE="$KERNEL_CMDLINE_PRE console=ttyHS4,115200n8"
#export APP_ADB=1

#No support??
#export KERNEL_INITRD_COMPRESSION="lzma -f"
